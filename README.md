Codely Plugin Documentation
===========================

Other languages are available: [PT-BR](https://gitlab.com/jcfaracco/codely-plugin-doc/tree/master/docs/pt-br)

The purpose of repository is answer all the questions related to:
1. Why does Codely need a plugin?
2. How does it work?
3. Which programming skills are needed?

Introduction
------------

Codely is a generic program built in Python and Javascript to generate code based on logic blocks. The main idea of Codely is provide an abstraction layer that anyone can create your own code structure according to blocks.

In other words, Codely has the same idea of a code generator. Very similar to a compiler. But this is not a compiler technically. Each block has it own strucutre that represents a code portion.

This code portion is responsible by each Plugin. If the user wants to generate blocks that corresponds to you language, he needs to develop his own Plugin.

[Blockly](https://developers.google.com/blockly/) has some built in languages but they are not available on Codely. Because if you want to try, you don't need Codely for this. You can just use the Blockly Sample called ["Code Editor"](https://blockly-demo.appspot.com/static/demos/code/index.html)